package busquedaenprofundidad;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jose
 */
public class ArbolBinario {
    
    Nodo raiz; 
  
    public ArbolBinario()
    { 
        raiz = null; 
    } 

    public void imprimirBusquedaEnProfundidad(Nodo nodo) 
    { 
        if (nodo == null) 
            return; 
        System.out.print(nodo.dato + " "); 
        imprimirBusquedaEnProfundidad(nodo.izquierda); 
        imprimirBusquedaEnProfundidad(nodo.derecha); 
    } 
   
    public void busquedaEnProfundidad() {
        imprimirBusquedaEnProfundidad(raiz);
    }
    
}