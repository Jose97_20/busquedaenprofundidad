package busquedaenprofundidad;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jose
 */

public class BusquedaEnProfundidad {
    
    public static void main(String[] args) {
        ArbolBinario nodo = new ArbolBinario(); 
        nodo.raiz = new Nodo(1); 
        nodo.raiz.izquierda = new Nodo(2); 
        nodo.raiz.derecha = new Nodo(3); 
        nodo.raiz.izquierda.izquierda = new Nodo(4); 
        nodo.raiz.izquierda.derecha = new Nodo(5); 
        nodo.raiz.izquierda.izquierda.izquierda = new Nodo(6); 
        nodo.raiz.izquierda.derecha.derecha = new Nodo(7);
        nodo.raiz.derecha.derecha = new Nodo(8);
  
        System.out.println("Busqueda en Profundidad"); 
        nodo.busquedaEnProfundidad(); 
    }
}